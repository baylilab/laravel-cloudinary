<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;

class UploadController extends Controller
{
    /**
	 * Construct untuk koneksi ke cloud
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct() {
		/**
		 * Set your APIKey Cloudinary
		 * cloudname
		 * api_key
		 * api_secret
		 */
		\Cloudinary::config(array(
			"cloud_name" => "baylilab",
			"api_key" => "549619437375373",
			"api_secret" => "hFKgBr6cZm1lS4l5czpUQLUdtI8",
		));
	}
	
	public function index()
	{
		$title = "Form upload";
		return view('form_upload', compact('title'));
	}
    
    /**
	 * Upload files to cloudinary
	 *
	 */
	public function upload(Request $request) {

		$judul_file = $request->get('image-name');
		$gambar = $request->file('image');
        $nama_file = Str::slug($judul_file);
		$upload = \Cloudinary\Uploader::upload($gambar, array("public_id" => $nama_file));
		return redirect('show/'.$nama_file);

		//dd($data);
	}

	public function show($param)
	{
		$url = cl_image_tag($param);
		$title = "Show image";
		return view('show_image', ['url' => $url, 'title' => $title]);
	}
}
