@extends('templates.main')
@section('title', $title)
@section('content')
<center>
	<form class="contact100-form validate-form" action="/upload" method="POST" enctype="multipart/form-data" >
		{{ csrf_field() }}
		<span class="contact100-form-title">
			Upload Gambar Anda
		</span>

		<div class="wrap-input100 validate-input" data-validate = "Image is required">
			<input class="input100" type="text" name="image-name" placeholder="Nama gambar" >
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-user" aria-hidden="true"></i>
			</span>
		</div>

		<div class="wrap-input100 validate-input" data-validate = "Image is required">
			<input class="input100" type="file" name="image" >
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-user" aria-hidden="true"></i>
			</span>
		</div>
		<div class="container-contact100-form-btn">
			<button class="contact100-form-btn">
				Upload To Cloudinary
			</button>
		</div>
	</form>
	</center>
	@stop